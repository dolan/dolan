var http = require('http'),
    sys = require('sys'),
    url = require('url'),
	youtube = require('youtube-feeds'), 
    stdin = process.openStdin(),
    external_name = 'node_hello',
    server = http.createServer(function (req, resp) {
		isAuthorized(doit, 'admin/queue', 'login');

		function isAuthorized(ok, good, bad) {
			var options = {
				'port': req.headers.host.split(':')[1],
				'path': '/_session',
				'headers': {'cookie': req.headers.cookie }
			};

			// logged in?
			http.get(options, function(res) {
				var str = '';

				res.setEncoding('utf8');
				res.on('data', function (chunk) {
					str += chunk;
				});
				
				res.on('end', function () {
					var o = JSON.parse(str);
					
					function rf(to) {
						resp.writeHead(302, {'Location':
							'http://' + req.headers.host + url.resolve(
							req.headers['x-couchdb-requested-path'], to
						)});
						resp.end();
					}
					
					// logged in?
					if (o && o.userCtx && o.userCtx.name) {
//						options.requested_path = req.headers['x-couchdb-requested-path'];
						options.db = req.headers['x-couchdb-requested-path'].split('/').slice(0, 2).join('/');
						ok(o, rf, good, options);
					} else {
						rf(bad);
					}
				});
			});
		}
	});

// We use stdin in a couple ways. First, we
// listen for data that will be the requested
// port information. We also listen for it
// to close which indicates that CouchDB has
// exited and that means its time for us to
// exit as well.
stdin.on('data', function(d) {
    server.listen(parseInt(JSON.parse(d)));
});

stdin.on('end', function () {
    process.exit(0);
});

// Send the request for the port to listen on.
console.log(JSON.stringify(['get', external_name, 'port']));

// Send a log message to be included in CouchDB's
// log files.
function cdblog(mesg) {
    console.log(JSON.stringify(['log', mesg]));
}

// code principal
function doit(o, cb, to, options) {
//	var requested_path = options.requested_path;
	var db = options.db;

	function yt(e, a) {

		var r, b = {docs:a.items}, req;


		for (r = 0; r < b.docs.length; ++r) {
			b.docs[r]._id = b.docs[r].id;
			delete b.docs[r].id;
		}

		//console.log(JSON.stringify(b, null, ' '));

		req = http.request(options, function(res) {
	//		console.log('STATUS: ' + res.statusCode);
	//		console.log('HEADERS: ' + JSON.stringify(res.headers));
			res.setEncoding('utf8');
			res.on('data', function (chunk) {
	//			console.log('BODY: ' + chunk);
			});
		});

		req.on('error', function(e) {
	//		console.log('problem with request: ' + e.message);
		});

		// write data to request body
	//	req.write('data\n');
	//	req.write('data\n');
		req.write(JSON.stringify(b, null, ' ') + '\n');
		req.end();

		cb(to);
	}

//	delete options.requested_path;
	delete options.db;
    cdblog('doit()');
    cdblog(JSON.stringify(o, null, ' '));

	options.headers['Content-Type'] = 'application/json';
	options.method = 'post';
//	options.path = requested_path.split('/').slice(0, 2).join('/') + '/_bulk_docs';
	options.path = db + '/_bulk_docs';
	
    cdblog('OPTIONS:');
    cdblog(JSON.stringify(options, null, ' '));

	youtube.user('mantaya001').uploads( yt );

}
