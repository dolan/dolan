TODO
====
  0) Mettre de l'ordre dans le TODO (grandes priorités)

  7) Google Analytics

  8) Améliorer le look admin

  9) Rendre plus générique

 11) Meilleur éditeur de texte/formulaire
     (remplacer \n par <br> ou <p></p>)

 13) Fonction externe pour importer de youtube, au lieu
     d'utiliser nodejs via couchdb tel que recommandé

 14) Intégrer grunt-lodash-cli pour précompiler les templates

 15) URL/id customs pour chaque vidéo

 16) Modal video (ajax), override le lien vers
     les pages de chaque vidéo quand possible

 17) Backend vimeo, etc. (comme on fait pour youtube)

 18) Mieux utiliser grunt (voir index.html par exemple) du côté
     des templates (en plus de grunt-lodash-cli)

19a) Catégories sur mesure
     (maintenant: compositions et interprétations seulement)

19b) Liste des vidéos par catégorie
     (présentement une seule liste contenant 2 catégories)

 20) Transformer liste admin dans un template html

 21) RSS/Atom feeds

 23) Validation d'update (sur édition d'un vidéo: bons champs, etc.)

 24) Installation/readme: bower install && npm install; grunt deploy...
 
 25) Footer

 26) Réviser le html de toutes les pages (inclusion du css, js, etc.)

DONE
====
  0) L'ordre est seulement un indicateur

  1) Ajouter update_validate function (droits à la db) puisque
     la gestion des droits (de l'interface) dans le couchapp est
     plutôt cosmétique.

 2a) Ajouter Bootstrap.

 2b) Reproduire le look qu'on avait Martin et moi.

 2c) Navigation (menu principal, login/logout)

  3) Taille des annonces Google (Responsive, beta option)

  4) Configurer vhost dans couchdb

 5a) Hoster sur couchdb gratis (iriscouch https://gr.millette.iriscouch.com:6984/ mais trop lent/instable)

 5b) Hoster sur digital ocean <http://www.martinantaya.com/>

 5c) Configurer NAT (port 5984 <--> 80)

  6) Nom de domaine martinantaya.com (propagating...)

  9) Remplacer les /ma2 et/ou _design/app hardcodés

10a) Mettre dans git (vérifier passwords, path et url hardcodés, etc.)

10b) Publier sur gitorious https://gitorious.org/dolan

 12) Meilleurs templates pour les vidéos, etc.

 22) Meilleure fonction d'authentification (basée sur rôle)

 25) Pages traduites (navigation, etc.)
