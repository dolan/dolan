/* * Les pages
 * =========
 * / (frontpage)
 * /fr and /en (maybe?)
 * /fr/rss
 * /en/rss
 * /fr/about
 * /en/about
 * /fr/contact
 * /en/contact
 * /fr/videos
 * /en/videos
 * /fr/videos/abc123
 * /en/videos/abc123
 * /admin
 * /admin/queue
 * /admin/show/abc123
 * /admin/edit/abc123
 * /login (move to admin/login?)
 * /logout link
 * /refresh (youtube) link
 * 
 */

var couchapp = require('couchapp'),
    path = require('path'),
	ddoc = {
  _id: '_design/app',
  language: 'javascript',
  validate_doc_update: function(newDoc, oldDoc, userCtx, secObj) {
	var isAnon = require('views/lib/misc').isAnon;

	if (isAnon(userCtx)) {
		throw({ unauthorized: "Login, why don't you." });
	}
  },
  rewrites: [{
    from: '',
    to: 'index.html'
  },{
    from: 'refresh',
    to: '../../../_hello'
  },{
    from: 'json/*',
    to: '../../*'
  },{
    from: 'login',
    to: 'login.html',
    method: 'GET'
  },{
    from: 'login',
    to: '../../../_session',
    // need absolute next path (URL)
    // see https://github.com/apache/couchdb/blob/fbc0545e39d7871afc469f3079dee67f823f3220/src/couchdb/couch_httpd_auth.erl#L286
	// move the query to the form action such as
	// action='/_session?next=/ma2/_design/app/_rewrite/admin'
//    query: {next: '/ma2/_design/app/_rewrite/admin'},
    query: {next: '/admin'},
    method: 'POST'
  },{
    from: 'admin/queue',
    to: '_list/admin/admin',
    query: {
		'descending': 'true',
		endkey: ['not now',''],
		startkey: ['not now','\ufff0']
	}	
  },{
    from: 'admin',
    to: '_list/admin/admin',
    query: {
		'descending': 'true'
	}
  },{
    from: 'fr/bio',
    to: 'bio.html'
  },{
    from: 'en/bio',
    to: 'bio-en.html'
  },{
    from: 'fr/contact',
    to: 'contact.html'
  },{
    from: 'en/contact',
    to: 'contact-en.html'
  },{
    from: 'fr/videos',
    to: '_list/videos/videos',
    query: {
		'descending': 'true',
		'lang': 'fr'
	}
  },{
    from: 'en/videos',
    to: '_list/videos/videos',
    query: {
		'descending': 'true',
		'lang': 'en'
	}
  },{
    from: 'fr/videos/*',
    to: '_show/video/*',
    query: {
		'lang': 'fr'
	}
  },{
    from: 'en/videos/*',
    to: '_show/video/*',
    query: {
		'lang': 'en'
	}
  },{
    from: 'admin/show/*',
    to: '_show/show/*',
    query: {}
  },{
    from: 'admin/edit/*',
    to: '_show/edit/*',
    method: 'GET'
  },{
    from: 'admin/edit/*',
    to: '_update/edit/*',
    method: 'POST'
  },{
    from: 'u/*',
    to: '../../../_users/*'
  },{
    from: '*',
    to: '*'
  }],
  views: {
	lib: couchapp.loadFiles('./lib'),
	admin: { map: function(doc) {
	  emit([doc.publish||'not now', doc.uploaded], {title:doc.title});
	}},
	videos: {'map':function(doc) {
		if (doc.publish && 'publish' === doc.publish && doc.category && doc.uploader && 'mantaya001' === doc.uploader && 'Music' === doc.category) {
		  emit([doc.type||'composition', doc.uploaded], {l:doc.l, description:doc.description, thumbnail:doc.thumbnail.hqDefault, duration:doc.duration});
		}
	}}
  },
  lists: {
	admin: function(head, req) {
		var row, t = req.query.startkey ? 'queue' : 'admin', u = req.query.startkey ? '../admin' : 'admin/queue',
			isAnon = require('views/lib/misc').isAnon;

		if (isAnon(req.userCtx)) {
			start({code:403, 'headers': {'Content-Type': 'text/html; charset=utf8'}});
			send('oups, <a href="' + (req.query.startkey? '../' : '') + 'login">login?</a>');
			return;
		}

		// TODO
		// turn into html template
		start({'headers': {'Content-Type': 'text/html; charset=utf8'}});
		send('<h1>' + t + '</h1>');
		send('<p>(see also <a href="' + u + '">' + u + '</a>)</p>');
		send('<ol>');
		while(row = getRow()) {
			send('<li><a href="' + ('queue' === t ? '../' : '') + 'admin/edit/' + row.id + '">' + row.value.title + '</a> ' + (req.query.startkey?'':('(' + row.key[0] + ')')) + '</li>');
		}		
		send('</ol>');
	},
	videos: function(head, req) {
		var type_str, last_type, row, tpl = require('views/lib/lodash-templates').templates;

		start({'headers': {'Content-Type': 'text/html; charset=utf8'}});
		send(tpl.html_begin({lang: req.query.lang}));
		send(tpl.videos_begin({lang: req.query.lang}));

		while(row = getRow()) {
			// switch list when row.key[0] (doc.type) changes
			if (row.key[0] !== last_type) {
				if (last_type) { send('</div>'); }//row
				// interpretation / composition (et bilingue).....
				if ('interpretation' === row.key[0]) {
					if ('fr' === req.query.lang) {
						type_str = 'Interprétations';
					} else {
						type_str = 'Interpretations';
					}
				} else {
					type_str = 'Compositions';
				}
				send('<h2>' + type_str + '</h2><div class="row">');
			}
			last_type = row.key[0];
			row.description = row.value.l[req.query.lang].description;
			row.title = row.value.l[req.query.lang].title;
			send(tpl.videos_loop(row));
		}	
		send('</div>');// row

		send('</div><div class="col-sm-4 col-md-3">');
		send('<!-- p>Ici vont les annonces...</p -->');

		send('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>\
<!-- Martin A -->\
<ins class="adsbygoogle"\
     style="display:block"\
     data-ad-client="ca-pub-4048374370961208"\
     data-ad-slot="1144656577"\
     data-ad-format="auto"></ins>\
<script>\
(adsbygoogle = window.adsbygoogle || []).push({});\
</script>');

		
		send('</div></div>');
		
		send(tpl.html_end());
	}
  },
  shows: {
	  edit: function(doc, req) {
		  var tpl = require('views/lib/lodash-templates').templates,
			isAnon = require('views/lib/misc').isAnon;

		  if (isAnon(req.userCtx)) {
			return {body:'oups, <a href="../../login">login?</a>', code:403, 'headers': {'Content-Type': 'text/html; charset=utf8'}};
		  }
		  
		  if (req.query.lang) { doc.lang = req.query.lang; }
		  return tpl.edit(doc);
	  },
	  show: function(doc, req) {
		  var tpl = require('views/lib/lodash-templates').templates,
			isAnon = require('views/lib/misc').isAnon;

		  if (isAnon(req.userCtx)) {
			return {body:'oups, <a href="../../login">login?</a>', code:403, 'headers': {'Content-Type': 'text/html; charset=utf8'}};
		  }
		  
		  doc.req = req;
		  return tpl.show(doc);
	  },
	  video: function(doc, req) {
		  var tpl = require('views/lib/lodash-templates').templates,
			isAnon = require('views/lib/misc').isAnon;
		  
		  if (req.query.lang) { doc.lang = req.query.lang; }
		  // give edit link to admin user only

		  if (isAnon(req.userCtx)) {
			doc.ed = '';
	      } else {
			  doc.ed = ' <small><a href="../../admin/edit/' + doc._id  + '?lang=' + doc.lang + '">edit</a>';		  
			  doc.ed += ' <a href="../../json/' + doc._id + '">json</a></small>';		  
		  }

		  return tpl.html_begin({lang: req.query.lang}) + tpl.show_video(doc) + tpl.html_end();
	  }
  },
  updates: {
	  edit: function(doc, req) { 
		var abs_path = '/', isAnon = require('views/lib/misc').isAnon;
		
		if ('_rewrite' === req.requested_path[3]) {
			abs_path += req.requested_path.slice(0, 4).join('/') + '/';
		}
		
		if (isAnon(req.userCtx)) {
			return [null,
				{
					body: 'oups, <a href="../login">login?</a>',
					code: 403,
					'headers': {'Content-Type': 'text/html; charset=utf8'}
				}];
		}
		  
		if (!doc.l) { doc.l = {}; }
		if (!doc.l.fr) { doc.l.fr = {}; }
		if (!doc.l.en) { doc.l.en = {}; }
		if (req.form.titre_fr) { doc.l.fr.title = req.form.titre_fr; }
		if (req.form.title_en) { doc.l.en.title = req.form.title_en; }
		if (req.form.description_fr) { doc.l.fr.description = req.form.description_fr; }
		if (req.form.description_en) { doc.l.en.description = req.form.description_en; }
		if (req.form.publish) { doc.publish = req.form.publish; }
		if (req.form.type) { doc.type = req.form.type; }

		return [doc, {code:302, headers:{Location: abs_path + (req.form.lang||'admin') + (req.form.lang?'/videos/':'/show/') + doc._id}}];
	  }
  }
};

couchapp.loadAttachments(ddoc, path.join(__dirname, 'dist'));

module.exports = ddoc;
