;(function() {
  var undefined;

  var objectTypes = {
    'function': true,
    'object': true
  };

  var root = (objectTypes[typeof window] && window) || this;

  var freeExports = objectTypes[typeof exports] && exports && !exports.nodeType && exports;

  var freeModule = objectTypes[typeof module] && module && !module.nodeType && module

  var moduleExports = freeModule && freeModule.exports === freeExports && freeExports;

  var freeGlobal = objectTypes[typeof global] && global;
  if (freeGlobal && (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal)) {
    root = freeGlobal;
  }

  var _ = root._;

  var templates = {};

  templates['allo'] = function(obj) {
    obj || (obj = {});
    var __t, __p = '', __e = _.escape;
    with (obj) {
    __p += '<div style="float: right; padding-left: 1em; padding-bottom: 1em;">\n\t<img src="' +
    __e( thumbnail.sqDefault ) +
    '" alt="thumbnail from video">\n</div>\nEDIT ' +
    __e( title ) +
    '<br>\n<p>' +
    __e( description ) +
    '</p>\n<form method="post" action="' +
    __e( _id ) +
    '">\ntitle (en): <input type="text" name="titre_en" value="' +
    ((__t = ( (('object' === typeof en) && en.title) ? en.title : title )) == null ? '' : __t) +
    '"><br>\ndescription (en): <input type="text" name="description_en"' +
    ((__t = ( (('undefined' !== typeof en) && en.description) ? (' value="' + en.description + '"') : '' )) == null ? '' : __t) +
    '><br>\n\n<hr>\ntitre (fr): <input type="text" name="titre_fr"' +
    ((__t = ( (('undefined' !== typeof fr) && fr.title) ? (' value="' + fr.title + '"') : '' )) == null ? '' : __t) +
    '><br>\ndescription (fr): <input type="text" name="description_fr"' +
    ((__t = ( (('undefined' !== typeof fr) && fr.description) ? (' value="' + fr.description + '"') : '' )) == null ? '' : __t) +
    '><br>\n<input type="submit">\n</form>\n';

    }
    return __p
  };

  if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
    define(['../lib/lodash.js'], function(lodash) {
      _ = lodash;
      lodash.templates = lodash.extend(lodash.templates || {}, templates);
    });
  } else if (freeExports && freeModule) {
    _ = require('../lib/lodash.js');
    if (moduleExports) {
      (freeModule.exports = templates).templates = templates;
    } else {
      freeExports.templates = templates;
    }
  } else if (_) {
    _.templates = _.extend(_.templates || {}, templates);
  }
}.call(this));
