;(function() {
		'use strict';
		
		exports.isAnon = function(userCtx) {
			return -1 === userCtx.roles.indexOf('ma');
		}
}());
